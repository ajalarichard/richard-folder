# richard-folder

# DOCUMENTATION FOR FINDING NON COMPLIANT BUCKET, FIX IT AND SEND AN SNS TOPIC TO YOUR EMAIL

# FIRST STEP
# CREATE CONFIG RULE

    1. Search for config in the AWS services search bar, click on settings on the left hand side of the dashboard and go to rules
    2. search for s3 rule you need in the aws managed rules and select the rule you want. In this case it is - "s3-bucket-level-public-access-prohibited"
    3. create an s3 bucket, an sns topic and a role for the config rule.
    4. In your sns topic, you have to create a subscription for the topic so your messages can go to the email you need
    5. create a role for your lambda function which will include policies for the config rule, write logs to cloudwatch, get access to your s3 buckets and sns topic


# Lambda Function
    1. In the lambda function, connect the already created role to the lambda function. 
    2. I created triggers for the lambda function. One of the triggers is to run the lambda function whenever an s3 bucket is created.
    3. declare and instantiate the variables that you will need to assign your boto3 s3 resources, sns client and config rule. 
    4. also declare and instantiate your config rule name
    5. in the lambda handler function, we set a variable to get the response from the config rule based on filtering with "NON_COMPLIANT"
    6. then we create an empty array to put the non_compliant buckets in so we can remediate them later
    7. next we do a try and exceot to get the non_compliant buckets appended to the array we created and if all the buckets are COMPLIANT
        we print a message stating that
    8. then we loop throught the non_compliant buckets we have in the array and remediate those buckets by setting some values to true
    9. after remediating the non_compliant buckets we send an email through the sns topic we created earlier with a subject line and message.

    
